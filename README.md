# Data

In the future we will provide a more static and final location for a final version of our data. This repository houses the data used in our models and experiments but the `code` repository maintains all the scripts needed for processing and formatting that data.

## clean

Generated files will reside here that are cleaned versions of documents and vocabularies.

## documents

JSON files of converted academic documents.

## raw

Raw files tracking the collection of data.

## results

MTURK results.

## synthetic

Some small sample datasets used for model checking.

## tasks

Tasks created for MTURK data.

## argument

Raw data from a recent work https://arxiv.org/abs/1705.00045. The data can be directly download from http://xinyuhua.github.io/Resources/. The focus of their work is slightly different from ours, but their data is very similar in spirit and format. The processed version of this dataset will reside in `results/arguement`.
