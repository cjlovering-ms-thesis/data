{
 "document_id": "1170673.1", 
 "sentences": [
  "Objective  \nThe purpose of this study was to determine how high school science teachers address  \ntheir Advanced Placement (AP) courses differently from their honors and regular-track courses.", 
  "A unique aspect of this research was having same teachers, who taught both an AP and either a  \nregular or honors course, consider both courses and indicate their beliefs, goals, and autonomy  \nthey possessed for each course type.", 
  "Perspective  \nAP course growth has been over 500% in the last two decades (Author, 2015).", 
  "Presence  \nof AP courses and achievement on AP exams are heralded as hallmarks of excellence by  \norganizations such as U.S. News World Reports (Morse, 2016a) and Washington Post (n.d.).", 
  "This  \ndrive to establish more AP courses is particularly acute in science and mathematics (Author, in  \npress).", 
  "This is evidenced by examples such as Google\u2019s STEM Access program focused on  \nincreasing participation in STEM AP courses (College Board, 2016a) as well as Texas Instrument  \nFoundation\u2019s contributions to the National Math and Science Initiative promoting STEM AP  \ncourses (Texas Instruments, n.d.).", 
  "Further evidence of attention on AP in STEM is U.S. News and  \nWorld Reports\u2019 calculation for determining the \u201cBest STEM High Schools\u201d which heavily  \nintegrates completion and success on STEM AP exams (Morse, 2016b).", 
  "Given that incredible credence is given to science AP courses and exams, it is important  \nto understand their workings.", 
  "The direction of this study was to understand science AP courses  \nby comparing them to non-AP science courses.", 
  "Teachers therefore were the units of analysis.", 
  "The interest was to understand how same teachers approached different course types.", 
  "From  \nthis, the following research question emerged:  \nIn what ways are science AP courses treated differently by teachers, as compared to honors  and regular-track courses?", 
  "Research points to teachers believing higher-track students more capable of engaging in  \nhigher-order thinking than lower-track students and that instruction should be differentiated  \naccordingly (Zohar, Degani, & Vaaknin, 2001).", 
  "However, Even and Kvatinsky (2009) found  \nsolitary teachers also bring common patterns of interactions to varied classes.", 
  "Additionally,  \nalthough researchers have examined AP course-taking and college success (e.g., Klopfenstein &  \n\fThomas, 2009; Sadler & Tai, 2007), there has been little investigation into how teachers actually  \ntreat AP courses differently than other courses.", 
  "The College Board states they do not mandate  \n\u201cany one curriculum for AP courses\u201d (College Board, 2016b).", 
  "Considering this combination of  \nvalue placed on AP science and the freedom afforded teachers, it is important to examine how  \nscience teachers address these courses as they pivot from AP to regular or honors.", 
  "Instrument  \nThe study\u2019s online survey prompted teachers to indicate their AP science course and a  \nnon-AP comparison course taught in the last three years.", 
  "Directions indicated comparison  \ncourses should be one covering similar topics as their AP course.", 
  "For example, an AP Biology  \nteacher might select Honors Biology or Regular Biology for comparison.", 
  "The main set of survey questions were 31 questions from the National Survey of Science  \nand Mathematics Education, conducted by Horizon Research (Banilower et al., 2013).", 
  "Questions elicited responses regarding (1) beliefs about teaching science, (2) control teachers  \nhave over curriculum, (3) emphasis placed on overarching goals (e.g., memorizing science  \nfacts), and (4) frequency of science practices (e.g., doing hands-on activities).", 
  "Respondents  \nindicated on Likert scales for their AP and comparison courses.", 
  "Figure 1 provides a question  \nformat example.", 
  "Due to formatting being altered from Horizon Research, validity measures  \nwere carried out.", 
  "_______________________  \nFigure 1   \n_______________________  \nEighty-five AP science teachers completed the survey (Table 1).", 
  "Teachers were solicited  \nthrough invitations sent to emails provided on school websites.", 
  "Thirty-nine teachers provided  \nan honors comparison course and 46 provided a regular-track comparison course.", 
  "The most  \ncommonly compared honors courses were Honors Biology and Honors Chemistry; regular  \n\fscience comparison courses listed most were Regular Biology, Regular Chemistry, and General  \nPhysics.", 
  "Analysis  \n_______________________  \nTable 1   \n_______________________  \nThe goal was to determine if teachers\u2019 responses were appreciably different for AP  \ncontrasted to comparison courses.", 
  "Because teachers responded to the same questions for two  \ncourses, these were considered paired sets.", 
  "After testing assumptions of symmetry, a series of  \nWilcoxon signed-rank tests were applied to determine significant differences among outlooks  \nand approaches for AP versus comparison courses.", 
  "Data were therefore examined as  \nindependent pairs \u2013 paired per teacher.", 
  "Beliefs   \nAP vs.", 
  "Regular.", 
  "For six of nine items, teachers held similar beliefs regarding AP and  \nregular science courses.", 
  "Science teachers did indicate significant differences (p < .05) in beliefs  \nthat students in regular courses require more explicit explanation and review than AP students.", 
  "This is reflected in significantly different ratings of \u201cteachers should explain ideas before  \nstudents consider evidence,\u201d and \u201cmost periods should include review of previous ideas/skills\u201d  \n(Table 2).", 
  "This speaks to viewpoints that regular-track students require more in-class structure.", 
  "However, teachers shifted views about structure when it came to homework.", 
  "Teachers held a  \nsignificantly stronger belief that AP students should receive daily homework versus reulgar- \ntrack students.", 
  "_______________________  \nTable 2   \n_______________________  \nAP vs.", 
  "Honors.", 
  "Teachers also held a significantly different view regarding the need to  \nreview previous ideas in AP versus honors courses.", 
  "This was similar to science teachers who  \ncompared AP to regular courses.", 
  "That is, although teachers agreed previous skills should be  \nreviewed ongoing, they believe there is less need for this in AP than in regular or honors  \ncourses.", 
  "The one other belief for which teachers rated honors courses significantly differently  \nthan AP was \u201cscience background inadequacies can be overcome with effective teaching.\u201d  \nTeachers indicated this was significantly truer in honors courses than in AP.", 
  "One possible  \nexplanation is teachers feeling they have greater control over materials and pedagogy of  \nhonors versus AP courses (see next subsection) consequently feel they have greater capacity to  \naddress students\u2019 background deficiencies in honors courses.", 
  "AP vs.", 
  "Regular.", 
  "Science teachers indicated relatively high, and equivalent, levels of  \ncontrol over choosing teaching techniques for both AP and regular courses, with means of 4.73  \nand 4.82, respectively.", 
  "Also equivalent, but considerably less in magnitude, was control over  \nselecting textbooks for AP (x\u0304=2.84) and regular courses  (x\u0304=2.86).", 
  "However, teachers indicated  \npossessing significantly more control in other areas in regular courses than in AP.", 
  "This included  \nability to select goals, topics, and grading criteria.", 
  "AP vs.", 
  "Honors.", 
  "AP-to-Honors control results were generally parallel AP-to-Regular  \nresults.", 
  "Like regular courses, honors courses apparently yield greater teacher independence  \nthan AP courses when it comes to structuring the class.", 
  "AP vs.", 
  "Regular.", 
  "A foreseeable finding was test-taking strategies emphasized significantly  \nmore in AP than regular or honors courses.", 
  "Beyond this, science teachers emphasize most goals  \nsimilarly in AP and regular courses (Table 3).", 
  "Consistency between courses included strong  \nemphases (x\u0304 \u2265 3.4) on learning about science process skills, real-life applications, and increasing  \ninterest in science.", 
  "Science teachers also expressed lower, but equivalent, emphasis on  \nmemorizing facts and vocabulary.", 
  "Aside from learning about test-taking strategies, the most  \n\fevident difference was emphasis placed on preparing students for further science study (p \u2264  \n.001, r = 0.50).", 
  "_______________________  \nTable 3   \n_______________________  \nAP vs.", 
  "Honors.", 
  "Significant differences on preparing students for further science study  \ndid not persist when teachers compared AP to honors.", 
  "Instead, teachers provided nearly  \nidentical strong ratings.", 
  "Other than the differences in learning test-taking strategies, teachers  \nplaced significantly greater emphasis on stimulating students\u2019 science interest in honors  \ncourses than in AP (p = .008) but also placed a slight but significantly greater emphasis on  \nunderstanding science concepts in AP courses (p = .008).", 
  "Frequency of Practices  \nAP vs.", 
  "Regular.", 
  "Teachers indicated large and significant differences in the amount of  \ntime in AP classes practicing for standardized tests compared to both regular and honors  \ncourses.", 
  "Although teachers reported most practices occur at similar rates in AP and regular  \ncourses, teachers indicated students spend significantly more time in AP, versus regular  \ncourses, participating in whole class discussions, analyzing data, and engaging in assessments  \nrequiring constructed responses.", 
  "AP vs.", 
  "Honors.", 
  "The significantly greater engagement of AP students with assessments  \nrequiring constructed responses was found again.", 
  "Also similar to the AP-to-Regular course  \npivot, science teachers more frequently engage AP students in whole class discussions than  \nhonors students.", 
  "Teachers also indicated in both AP and honors classes little time is spent  \nreading science material (x\u0304 = 2.33 and 2.05, respectively).", 
  "Otherwise, science teachers indicated  \ncomparable frequency of practices in AP and honors.", 
  "Implications and Significance  \nThough not surprising to learn science teachers treat distinctive courses differently and  \nhave varying goals for those courses, this study yielded signs pointing to the most pronounced  \n\fdissimilarities.", 
  "In this discussion section, attention is drawn largely to significant differences (p <  \n.05), where absolute difference of average scores was at least 0.4, and there was at least a  \nmedium effect size (r \u2265 0.3) (Cohen, 1988).", 
  "There were many similarities between what a teacher believed should occur in AP and  \nin comparison courses.", 
  "However, it was found that teachers believe more firmly AP students  \nshould be provided daily homework versus regular-track students.", 
  "While the value of  \nhomework is controversial (e.g., Trautwein & K\u00f6ller, 2003), this finding brings up questions  \nregarding why higher ability students receive homework more regularly.", 
  "One possibility is  \nteachers are reticent to assign homework often in regular-track courses if they feel students  \nwon\u2019t complete the homework at the same satisfactory rate as higher ability students.", 
  "If true,  \nthen homework may be tied to teachers\u2019 expectation of success and cost (i.e., time) of  \nmanaging assignments.", 
  "Teachers also strongly felt \u201cscience background inadequacies can be overcome with  \neffective teaching\u201d significantly more in honors than AP courses.", 
  "This warrants further  \ninvestigation, but may relate to greater freedom in honors courses.", 
  "This greater autonomy  \npossibly supports science teachers\u2019 abilities to modify and adapt instruction in honors opposed  \nto AP and regular courses where tighter scopes and sequences must be followed.", 
  "Regarding goals, understandably, \u201clearning test-taking strategies\u201d was significantly  \ngreater for AP versus comparison courses.", 
  "Science teachers also held a stronger goal of  \npreparing AP students for further science study than they did for regular students.", 
  "This may be  \ndue to students\u2019 interests, as interpreted by the teachers, or may be more aligned to teachers  \npredetermining goals for sets of courses.", 
  "Regarding practices, students prepare for tests far more often in AP courses than  \ncomparison courses.", 
  "Otherwise, science practices are generally standard across courses.", 
  "Notable exceptions are AP students encountering tests requiring open-ended responses  \nsignificantly more than regular or honors students; and regular students analyzing data  \nsignificantly less than AP students.", 
  "While AP content may be \u201charder\u201d than regular-track this  \ndoes not substantiate that AP students are prompted more often to demonstrate their thinking  \nthrough constructed responses and analysis of data.", 
  "A conclusion drawn from autonomy data is teachers feel they have far less control over  \nAP courses than over regular and honors courses.", 
  "This is a significant finding since teacher  \nretention has been related to a sense of professionalism underpinned by autonomy (Boyd et al,  \n\nImplications   \nTwo takeaways are highlighted.", 
  "First, these results caution when teachers are surveyed  \nregarding dispositions.", 
  "Attitudes are not fixed data points.", 
  "Important to learn is mindset and  \napproach can vary dependent upon students being considered.", 
  "While not occurring for all  \nitems, there were abundant cases of science teachers rating AP courses differently than regular  \nor honors courses.", 
  "Researchers must take caution to ensure evaluations of attitudes about  \nstudents are not so holistic that they miss viewpoints teachers hold about specific student  \ngroups, particularly based on course level.", 
  "Second, while it is given that different course levels are associated with different topics,  \npace, and objectives, a second implication of this study is that it raises thought about general,  \nand sometimes surprising, similarities, but also about significant differences between courses.", 
  "For example, AP students are far more likely to be analyzing data than regular-track students.", 
  "It  \nis generally considered that valued practices should occur in all levels of science.", 
  "However,  \nrather than being reproachful and insinuate teachers withhold, or limit, some practices among  \nregular-track students, it is important to understand dynamics that lead to contrasts and  \nsubsequently assess if the current state is desirable or if changes are preferred."
 ]
}