{
 "document_id": "13747507431765682213", 
 "sentences": [
  "10.1177/0013164404273945 Educational and Psychological Measurement Nietfeld et al.", 
  "/ Relative and Absolute Monitoring Accuracy \nA Monte Carlo Comparison of Measures of Relative and Absolute Monitoring Accuracy \nEducational and Psychological Measurement Volume 66 Number 2 April 2006 258-271 \u00a9 2006 Sage Publications 10.1177/0013164404273945 http://epm.sagepub.com hosted at http://online.sagepub.com \nJohn L. Nietfeld North Carolina State University Craig K. Enders University of Nebraska\u2013Lincoln Gregory Schraw University of Nevada\u2013Las Vegas \nResearchers studying monitoring accuracy currently use two different indexes to esti- mate accuracy: relative accuracy and absolute accuracy.", 
  "The authors compared the distri- butional properties of two measures of monitoring accuracy using Monte Carlo proce- dures that fit within these categories.", 
  "They manipulated the accuracy of judgments (i.e., chance level or 60% and above) and the number of items per test (i.e., 20, 50, or 1,000) using 10,000 computer-generated cases.", 
  "Gamma, an estimate of relative accuracy, yielded a skewed, leptokurtic distribution under the 50-item, 60% accuracy conditions.", 
  "The Hamann coefficient, an estimate of absolute accuracy, yielded a normal distribution under the same conditions.", 
  "Both statistics yielded normal distributions under the 1,000- item, 60% conditions, although parameter estimates differed widely.", 
  "The two statistics were similar, and normally distributed, under the 50- and 1,000-item, chance conditions.", 
  "Recommendations are made regarding the use of each measure in applied monitoring accuracy research.", 
  "Keywords: monitoring accuracy; metacognition; calibration; Monte Carlo \nResearchers in a number of disciplines are interested in how well individuals moni- \ntor their learning and performance.", 
  "Monitoring is an important metacognitive skill that enables learners to assess performance and select appropriate fix-up strate- gies given an error (Baker, 1989; Schraw & Moshman, 1995).", 
  "It is generally consid- ered to be a data-driven dimension of metacognition that informs higher level control processes (Nelson & Narens, 1994).", 
  "Individuals who cannot monitor accurately can- \nAuthors\u2019 Note: Correspondence concerning this article should be addressed to John L. Nietfeld, North Carolina State University, Department of Curriculum & Instruction, 602D Poe Hall, Raleigh, NC 27695; e- mail: john_nietfeld@ncsu.edu.", 
  "Nietfeld et al.", 
  "/ Relative and Absolute Monitoring Accuracy \n\nnot correct errors and as a consequence process information less efficiently than skilled monitors (Pressley & Ghatala, 1990).", 
  "Therefore, metacognitive monitoring is at the core of many current models of self-regulation (Butler & Winne, 1995; Winne, \nAt least four types of metacognitive judgments have been used to operationalize the concept of monitoring to reflect the ongoing process-related metacognitive activities that one may engage in when performing a task.", 
  "These judgments include (a) confi- dence judgments (Schraw, 1995), (b) learning and comprehension monitoring or the judgment of learning (Koriat, 1997), (c) the feeling of knowing (Hart, 1965; Nelson, 1984), and (d) task difficulty or the ease of learning judgment (Underwood, 1966).", 
  "Studies using these various measures of monitoring judgments have made important advances with regard to understanding variables that affect monitoring accuracy.", 
  "For instance, monitoring accuracy may be affected by test difficulty (Presley & Ghatala, 1988; Schraw & Roedel, 1994), age (Oli & Zelinski, 1997), comprehension instruc- tion (Magliano, Little, & Graesser, 1993), background knowledge (Nietfeld & Schraw, 2002), and performance level (Maki & Berry, 1984).", 
  "Increases in monitoring accuracy have been shown by extending the length of a test (Weaver, 1990), increasing the information to be learned (Commander & Stanwyck, 1997), introducing strategy instruction prior to testing (Nietfeld & Schraw, 2002), and increasing processing demands while learning information (Maki, Foley, Kajer, Thompson, & Willert, 1990).", 
  "In general, research has shown that individuals who monitor their own under- standing during the learning phase of an experiment display better recall performance when their memories are tested (Nelson, 1996).", 
  "The indexes chosen to measure monitoring judgments can be classified as measur- ing either relative accuracy or absolute accuracy.", 
  "Relative accuracy is the accuracy of predicting performance on one item relative to another item (Nelson, 1996), whereas absolute accuracy is the extent to which an individual is calibrated with regard to his or her monitoring judgment and the criterion task (Schraw, 1995).", 
  "Currently, there does not appear to be a set of heuristics to guide monitoring researchers in their choices of measurement indexes.", 
  "We believe that the choice of either a relative or absolute mea- sure of monitoring accuracy should be based on the context in which the measurement occurs, taking into account the primary goals of the study.", 
  "For instance, if a researcher is primarily interested in the extent to which an individual makes consistent judgments across items, a measure of relative accuracy may be appropriate.", 
  "On the other hand, if a researcher is interested in changes in accuracy related to an intervention, training, or practice effects, a measure of absolute accuracy may be appropriate.", 
  "In addition, other constraints related to available sample size, the difficulty of the criterion task, and the expectation for normally distributed data also factor into one\u2019s choice of an index.", 
  "One commonly used measure of relative accuracy is \u03b3 (Nelson, 1984), an index of association developed by Goodman and Kruskal (1954).", 
  "Specifically, \u03b3 provides a mea- sure of association between two ordered, symmetric responses of two or more levels each.", 
  "Responses are ordered when they can be ranked along a single dimension; they are symmetric when there is no implied causal or temporal relation between them (Good- man & Kruskal, 1954).", 
  "Educational and Psychological Measurement \nA 2 \u00d7 2 Data Array Used in Calibration Research \n\nRecognition Performance (X) \nMarginals \nFeeling of \nMarginals \na + b + c + d \nThroughout the 1980s, \u03b3 was the statistic of choice among monitoring accuracy researchers.", 
  "Gamma has been used extensively to measure the relationship between the recognition of an event (i.e., yes or no) and the feeling of knowing for that event (i.e., yes or no).", 
  "Figure 1 provides an example of a typical 2 \u00d7 2 data array summariz- ing such judgments.", 
  "Four mutually exclusive outcomes are possible in this array.", 
  "Cell a consists of responses that are correct and judged as correct.", 
  "Cell b consists of incorrect responses that are judged as correct.", 
  "Cell c consists of correct responses that are judged as incor- rect.", 
  "Cell d consists of incorrect responses that are judged incorrect.", 
  "Data arrays of higher dimensionality are common in the current literature.", 
  "We present a 2 \u00d7 2 array for ease of interpretation and for the purpose of comparison in this study.", 
  "where HC is the Hamann coefficient.", 
  "Equation 1 reveals that \u03b3 is defined in terms of the four cell frequencies included in Figure 1.", 
  "Gamma ranges in value from \u20131 to +1 and is interpreted as the degree to which predicted performance on one set of variables corre- sponds to actual performance on the same set of variables (Nelson, 1984).", 
  "Thus, \u03b3 pro- vides a measure of relative accuracy that enables researchers to make inferences about the bivariate association between two sets of variables (Gibbons, 1993).", 
  "Nietfeld et al.", 
  "/ Relative and Absolute Monitoring Accuracy \n\nSchraw (1995) proposed an alternative measure of monitoring accuracy that esti- mates absolute accuracy and is shown in equation 2.", 
  "Romesburg (1984) referred to equation 2 as the Hamann coefficient and noted that it provides a measure of calibra- tion accuracy between performance and confidence on an item-by-item basis.", 
  "In a general sense, this measure of calibration reports the proportion of accurate versus inaccurate judgments for a task.", 
  "Accurate calibration is not common even among adults, and overconfidence increases as tests become more difficult (Schraw & Roedel, 1994).", 
  "Pressley, Yokoi, van Meter, Van Etten, & Freebern (1997) reported that adults\u2019 failure to monitor text comprehension and learning is common.", 
  "Comparing the two measures, Wright (1996) concluded that the primary difference between them is that \u03b3 relies on a multiplicative model, whereas the Hamann coeffi- cient relies on an additive model.", 
  "From a computational standpoint, \u03b3 uses the values within the four cells independent of marginal values.", 
  "The Hamann coefficient uses the values within the four cells relative to marginal values.", 
  "From an interpretative stand- point, \u03b3 provides a measure of the association between two sets of scores analogous to a nonparametric correlation, whereas the Hamann coefficient provides a measure of the difference between correct and incorrect judgments relative to all judgments.", 
  "Thus, \u03b3 provides a measure of association, whereas the Hamann coefficient provides a measure of the likelihood of correct judgments exceeding incorrect judgments.", 
  "Previous comparisons of \u03b3 and the Hamann coefficient have failed to provide a con- vincing case that one is best suited across all monitoring research.", 
  "Some researchers favor \u03b3 because it is margin independent (Nelson, 1984; Wright, 1996).", 
  "Others favor the Hamann coefficient because it more closely reflects the intuitive notion of judg- ment accuracy than does \u03b3 (Schraw, 1995).", 
  "In comparing the two, Schraw (1995) con- cluded that researchers should report both measures because the two are neither computationally nor interpretatively redundant.", 
  "In sum, it is important to consider the fundamental distinction between the two indexes when choosing monitoring accu- racy: Gamma is a measure of relative accuracy, whereas the Hamann coefficient is a measure of absolute accuracy (Nelson, 1996).", 
  "The purpose of the present research was to examine the distributional properties of \u03b3 and the Hamann coefficient using Monte Carlo techniques.", 
  "Our main reason for doing so was to examine each statistic\u2019s actual performance under typical and random experimental conditions.", 
  "These comparisons will enable researchers to assess the degree to which either statistic yields a normally distributed sampling distribution and facilitate comparisons that enable researchers to make informed judgments about when to use each statistic.", 
  "Presently, many researchers test \u03b3 using parametric tests based on the normal distribution, such as t and F (Thompson & Mason, 1996; Weaver, 1990).", 
  "If the sampling distribution does not adhere to a normal distribution, conduct- ing parametric significance tests may compromise the validity of these experiments.", 
  "We compared \u03b3 and the Hamann coefficient under six different hypothetical scenar- ios in which we manipulated the accuracy of the test taker (henceforth \u201ctest accuracy\u201d) and the number of items per test (henceforth \u201ctest length\u201d).", 
  "Test accuracy was manipu- lated by specifying constraints on the proportion of entries in each of the four cells in Figure 1.", 
  "One condition allowed these proportions to vary freely (i.e., the chance- \n\nEducational and Psychological Measurement \naccuracy condition that simulated random guessing); the other specified that at least 60% of all entries appear in cell \u201ca\u201d (i.e., the high-accuracy condition that simulated accurate calibration).", 
  "This manipulation enabled us to evaluate the distributional per- formance of \u03b3 and the Hamann coefficient in high- and low-skill environments.", 
  "The test length factor differed with respect to the number of judgments in each of 10,000 simulated trials and consisted of three levels.", 
  "The first two conditions included 20 and 50 judgments, respectively.", 
  "These conditions attempted to simulate typical experiments in which individuals complete from 10 to 50 test items.", 
  "Few experiments have exceeded this number of items; most are based on 10 to 20 test items.", 
  "The third condition included 1,000 judgments per trial.", 
  "This condition provided an estimate of asymptotic performance for each of the two test statistics.", 
  "One would expect distribu- tional parameters to be highly stable under the 1,000-judgment condition.", 
  "These three manipulations led to six distinct scenarios that fully crossed the two accuracy condi- tions with the three test length conditions.", 
  "We made two general predictions regarding these scenarios.", 
  "The first was that the two measures would not differ regarding four distributional parameters (i.e., mean, standard deviation, kurtosis, and skewness) in the chance-accuracy conditions (i.e., Scenarios 3 and 4).", 
  "As shown by Schraw (1995), \u03b3 and the Hamann coefficient should yield identical estimates when there is no relationship between recognition and moni- toring judgments.", 
  "Our second prediction was that significant differences would occur in the high-accuracy condition.", 
  "We predicted that \u03b3 would yield a negatively skewed, leptokurtic distribution, whereas the Hamann coefficient would not.", 
  "We expected neg- ative skewness because \u03b3 yields a truncated distribution on the upper end because of the restriction of range while producing a variety of extreme values on the lower end (Schraw, 1995).", 
  "We expected a leptokurtic distribution because \u03b3 yields a large num- ber of scores that are condensed into the upper end of the distribution.", 
  "We expected the Hamann coefficient to yield fewer extreme scores because it provides a marginalized estimate of monitoring accuracy (i.e., scores are conditionalized on observed margin \n\nData Generation \nWe created six separate 2 \u00d7 2 data matrices typical of the monitoring paradigm.", 
  "In doing so, two design factors were completely crossed: the number of test items (20, 50, and 1,000) and the accuracy of monitoring (chance vs. accurate).", 
  "Responses to simulated test items were categorized on two dimensions in each matrix, recognition of an event (i.e., yes or no) and the feeling of knowing for the event (i.e., yes or no), to create four mutually exclusive outcomes (see Figure 1).", 
  "Because it was of interest to examine the asymptotic behavior of the two indexes, 10,000 cases were generated within each of the six design cells.", 
  "For each case, a vector of random numbers between 0 and 1 was generated from a uniform distribution using the uniform random number generator in the SAS IML \n\fNietfeld et al.", 
  "/ Relative and Absolute Monitoring Accuracy \n\nprocedure; the number of test items in a given design cell defined the vector length.", 
  "To simulate a sample of high-accuracy monitors, approximately 60% of the item responses for each observation were placed into cell a, responses that are correct and judged as correct.", 
  "Next, a single response was assigned to each of the three empty cells to ensure that no cell was empty.", 
  "The remaining responses were then randomly assigned to the four cells of the 2 \u00d7 2 table using quartile cut points that partitioned the continuous uniform variates into four approximately equally probable sections.", 
  "For example, if a case had a value of 0.57, it would be assigned to cell \u201cc\u201d because it fell in the third quartile of the uniform distribution.", 
  "The chance-accuracy data were simu- lated by assigning a single item response to each of the four cells in the array, again ensuring no empty cells.", 
  "The remaining responses were then randomly assigned to the four cells using the quartile cut points of the distribution.", 
  "For each of the six design conditions described above (3 test lengths \u00d7 2 accuracy conditions), the \u03b3 statistic and Hamann coefficient were computed for each of the 10,000 cases.", 
  "Thus, the entire data generation process produced 12 separate sampling distributions.", 
  "We computed the overall mean of \u03b3 and the Hamann coefficient for each of the 12 sample distributions, along with the standard deviation, skewness, kurtosis, and range.", 
  "Within each of the two accuracy conditions, a 3 \u00d7 2 split-plot analysis of variance (ANOVA) was conducted to test for mean differences in the sampling distributions for the two coefficients.", 
  "The three levels of the test length variable served as the between- subjects factor, and the two coefficients served as the within-subjects factor.", 
  "Because each distribution included 10,000 cases, statistical significance tests are sensitive to potentially trivial differences.", 
  "As such, the \u03b72 effect size measure was computed for all design effects.", 
  "According to Cohen\u2019s (1988) recommendations, values of .01, .06, and .14 can roughly be interpreted as small, medium, and large effect sizes, respectively.", 
  "For the distributional parameters, it was of interest to compare the skewness and kurtosis values for each coefficient with that of a normal distribution (skewness = 0, kurtosis = 0).", 
  "To this end, a one-sample z statistic was formed using the standard error values for each parameter estimate, and the z statistics for both measures were subse- quently converted to values.", 
  "High-Accuracy Condition \nTable 1 shows estimates of the mean, standard deviation, skewness, kurtosis, and range for the \u03b3 and Hamann coefficient distributions under the high-accuracy simula- tions.", 
  "The 20- and 50-item conditions closely approximated the typical setting of most psychological experiments (Glenberg, Sanocki, Epstein, & Morris, 1987; Maki & Serra, 1992; Pressley & Ghatala, 1988; Thompson & Mason, 1996; Weaver, 1990), \n\nEducational and Psychological Measurement \nand the 1,000-item test provided insight into the performance under idealized con- \nOne clear difference in Table 1 is that the \u03b3 distribution yielded a substantially higher mean estimate of monitoring accuracy than the Hamann coefficient.", 
  "To further explore these differences, a 3 \u00d7 2 split-plot ANOVA was conducted to test for mean differences in the sampling distributions of the two coefficients.", 
  "Recall that the three test lengths served as levels of the between-subjects factor, and the two coefficients (\u03b3 and Hamann) constituted the within-subjects factor.", 
  "Because of the large number of replications (10,000), both main effects and the interaction term were statistically sig- nificant (p < .0001), so values were used to guide substantive interpretations.", 
  "Not surprisingly, the within-subjects main effect between the two coefficients yielded a large effect size (\u03b72 = .607).", 
  "The effect size estimates for the interaction and between-subject main effect were both small (\u03b72 = .011 and .049, respectively).", 
  "The small interaction effect suggests that the mean difference between the two coefficients remained fairly stable as the test length increased.", 
  "As seen in Table 1, mean values for both coefficients increased slightly with test length, although the trend for the \u03b3 coeffi- cient was somewhat more pronounced.", 
  "For example, the mean difference in the 20- item test condition was .1140 (.715 and .601 for \u03b3 and the Hamann coefficient, respec- tively), whereas the mean difference in the 1,000-item condition was .137 (.786 and .649, respectively).", 
  "This finding of higher estimates of monitoring accuracy by \u03b3 due to marginal independence is consistent with previous findings in the literature (Agresti, 1990; Freeman, 1987; Reynolds, 1977).", 
  "As seen in Table 1, the two coefficients also differed substantially with respect to distribution shape.", 
  "For the 20-item test, \u03b3 yielded a negatively skewed sampling distri- bution, while the Hamann coefficient approached a normal distribution with slight platykurtosis.", 
  "In this situation, it was of interest to compare the observed skewness and kurtosis values for each coefficient with those of a normal distribution (skewness = 0, kurtosis = 0).", 
  "Given the large sample size used in this study (N = 10,000), statistical tests of normality would have been far too sensitive.", 
  "Instead, a one-sample z statistic was formed using the standard error values for each parameter (SEskewness = 0.024, SEkurtosis = 0.049), and these test statistics were converted into effect size measures.", 
  "On the basis of Cohen\u2019s (1988) benchmark values, the skewness statistic for the \u03b3 statistic (skewness = \u20131.01, SE = 0.024) was small relative to that of a normal curve (\u03b72 = .04), and the kurtosis value did not exceed \u03b72 = .01.", 
  "The Hamann coefficient yielded no dis- tributional differences that exceeded the small benchmark.", 
  "For the 50-item test, \u03b3 yielded a negatively skewed, leptokurtic sampling distribu- tion, whereas the Hamann coefficient yielded a normal distribution.", 
  "For \u03b3, the skew- ness (skewness = \u20131.58, SE = 0.024) and kurtosis (kurtosis = 3.66, SE = 0.049) param- eters differed considerably from a value of zero for a normal distribution.", 
  "Both of these values reflected a medium effect size by Cohen\u2019s (1988) standards (\u03b72 = .10 and .12 for skewness and kurtosis, respectively).", 
  "In contrast, values for the Hamann coeffi- cient parameters were negligible (i.e., < .01).", 
  "Negative skewness in the \u03b3 distribution was associated with a number of extreme values that theoretically are difficult to explain under the present conditions.", 
  "For example, a value of \u20130.31 was obtained in \n\fNietfeld et al.", 
  "/ Relative and Absolute Monitoring Accuracy \nMeans, Standard Deviations, Skewness, Kurtosis, and Range of the \u03b3 and Hamann Coefficient Distributions \nAccurate Monitoring \nChance Monitoring \nTest Length \n1,000 items \nthe \u03b3 distribution even though accurate monitoring judgments always outnumbered inaccurate judgments by at least a 2:1 ratio given the constraints described earlier.", 
  "These extreme values can be attributed to unique computational properties of \u03b3, in which it is possible to obtain theoretically unexpected values (Schraw, 1995).", 
  "In contrast, fewer differences were observed between the \u03b3 and Hamann coefficient sampling distributions for the 1,000-item test.", 
  "Neither statistic yielded a nonnormal sampling distribution; the largest effect size value for the skewness and kurtosis parameters was associated with the \u03b3 distribution\u2019s skewness statistic (\u03b72 = .01), and the remaining effect size estimates were trivial.", 
  "The observed range for both statistics was reduced substantially because of test length.", 
  "In addition, \u03b3 did not yield theoreti- cally unexpected values, as it did in the 50-item test scenario, because extreme scores become less likely as the number of items on a test increases.", 
  "Finally, the variability of the \u03b3 distribution across the three test conditions was roughly twice that of the Hamann coefficient.", 
  "Given that the range of the Hamann dis- \n\nEducational and Psychological Measurement \ntribution spanned between four to seven standard deviation units, it does not appear to be the case that the Hamann coefficient artificially restricts the possible range of val- ues that can be obtained.", 
  "Rather, variability differences appear to be due to inherent efficiency (i.e., sampling variability) differences between the two coefficients.", 
  "The fact that a 2:1 ratio of the coefficients\u2019 standard deviations was maintained across the three test lengths further supports this contention.", 
  "In actual testing situations, this means that for any group of examinees with the same underlying level of accuracy, the Hamann coefficient appears to yield more reliable estimates of accuracy and is not as influenced by random sources of error as the \u03b3 statistic.", 
  "This difference can be attrib- uted to the fact that \u03b3 provides an unmarginalized estimate of monitoring accuracy.", 
  "Collectively, these differences suggest that the use of \u03b3 leads to inflated accuracy estimates and substantially more variability in the observed distribution of values than the Hamann coefficient.", 
  "Under realistic testing conditions, \u03b3 yielded theoretically unexpected values and did not conform to the parameters of a normally distributed sampling distribution.", 
  "As discussed below, this suggests that significance tests of \u03b3 based on the normal distribution may be inappropriate, leading to misleading conclu- sions.", 
  "In contrast, the Hamann coefficient yielded a normally distributed sampling dis- tribution, and all values fell within theoretically acceptable limits.", 
  "Given the cell con- straints used in the simulation, a reasonable range of scores was observed (e.g., six deviation units) that were dispersed around the expected center of the distribution.", 
  "It may be somewhat counterintuitive that the \u03b3 sampling distribution became more nonnormal in shape as the number of test items increased from 20 to 50.", 
  "However, this finding was likely an anomaly due to the simulation restraints described earlier.", 
  "Recall that approximately 60% of the item responses were assigned to cell \u201ca\u201d of the 2 \u00d7 2 contingency table.", 
  "Obviously, far fewer dispersion patterns were possible for the remaining item responses in the 20-item test condition than the 50-item condition.", 
  "Thus, the range of possible values was artificially restricted somewhat for both coeffi- cients.", 
  "With real rather than simulated data, it seems reasonable to expect that that per- formance of \u03b3 would consistently degrade as the number of items decreased.", 
  "Chance-Accuracy Condition \nAs before, mean differences in the sampling distributions for the two coefficients were examined using a 3 \u00d7 2 split-plot ANOVA.", 
  "In this case, no design effects approached statistical significance, and all effect size estimates were nil (\u03b72 < .0001).", 
  "This is not surprising in light of Table 1, which shows that the sampling distributions for both coefficients were centered at zero for the three test-length conditions.", 
  "Table 1 shows few differences between sampling distributions other than the mag- nitude of the variability.", 
  "Consistent with the accurate monitoring results, the variabil- ity of the \u03b3 sampling distribution was roughly double that of the Hamann coefficient.", 
  "As noted above, this difference is substantively important, because it reflects inherent differences in the precision (i.e., efficiency) of accuracy estimates between coeffi- cients.", 
  "That is, for any group of examinees sharing a similar level of accuracy, it is less likely to obtain outlier accuracy estimates because of random sources of error.", 
  "Finally, \n\fNietfeld et al.", 
  "/ Relative and Absolute Monitoring Accuracy \n\nnote that all sampling distributions for the chance accuracy condition were normally distributed; values for the skewness and kurtosis parameters were uniformly less than \nTesting \u03b3 for Statistical Significance \nAs noted earlier, it is not uncommon to test \u03b3 for statistical significance using nor- mal curve approximations (e.g., Thompson & Mason, 1996; Weaver, 1990).", 
  "Siegel and Castellan (1988) gave an expression for an upper bound estimate of the variance for \u03b3 that can be used to conduct significance tests using the z statistic.", 
  "However, in light of the earlier results concerning distribution shape, this practice may not yield accurate results.", 
  "To further investigate this issue, the z statistic outlined by Siegel and Castellan (1988) was computed for each of the 10,000 \u03b3 values across the design cells, and the percentage of statistically significant \u03b3 values was computed (p < .05).", 
  "For the accurate monitoring condition, the percentages of statistically significant results based on the z statistic were 47%, 70.7%, and 100.0% for the 20-, 50-, and 1,000-item tests, respec- tively.", 
  "For comparative purposes, the percentage of \u201ctrue\u201d rejections can be computed from the simulation results above.", 
  "Specifically, the sampling distributions from the chance-accuracy simulations correspond to the null hypothesis sampling distribu- tions.", 
  "By computing the percentage of \u03b3 values from the accurate monitoring condi- tion that exceeded the 95th percentile of the empirical null distribution, the true power for \u03b3 can be obtained.", 
  "On the basis of the simulation results presented above, the per- centages of \u03b3 values from the accurate monitoring condition that exceeded the 95th percentile of the null distribution (chance accuracy) were 76.9%, 93.2%, and 100.0%.", 
  "Siegel and Castellan (1988) noted that the z approximation for the \u03b3 statistic is con- servative, because the expression for the variance is an upper limit.", 
  "Results from this simulation confirmed that the test of significance for \u03b3 is extremely conservative under realistic testing conditions seen in monitoring research and will yield high rates of Type II errors.", 
  "For example, in the 20-item accurate monitoring condition, the z test yielded Type II error rates that more than doubled the expected rate.", 
  "It was only under unrealistically long testing conditions that the power of the z approximation matched the true power computed from the empirical sampling distributions.", 
  "Although there are currently no statistical significance tests for the Hamann coeffi- cient, it is informative to note that 100% of the coefficients from the three accurate monitoring conditions exceeded the 95th percentile of the null distribution (chance monitoring) for the Hamann coefficient.", 
  "That is, the true power for this statistic was unity for the conditions examined here.", 
  "Although formal statistical tests are not con- ducted, values of the Hamann coefficient are used to make judgments about monitor- ing accuracy that are akin to null or nonnull decisions.", 
  "Consistent with the previous discussion of variability differences between coefficients, this implies that the Hamann coefficient will yield more reliable and accurate assessments for a given group of examinees with similar levels of underlying accuracy.", 
  "This finding is particu- larly interesting in light of the fact that \u03b3 values were positively biased, which intu- \n\nEducational and Psychological Measurement \nitively would lead to greater separation between the null and noncentral distributions; this was not the case, however.", 
  "Discussion \nCurrently, researchers investigating monitoring ability use two distinct types of measures of monitoring accuracy, one for relative accuracy and the other for absolute accuracy.", 
  "The purpose of the present research was to compare the distributional prop- erties of representative indexes of each of these types of monitoring accuracy.", 
  "There- fore, a Monte Carlo simulation was conducted to allow for a direct comparison between \u03b3, the measure of relative accuracy, and the Hamann coefficient, the measure of absolute accuracy, under a variety of different conditions.", 
  "Results from the computer-generated distributions confirmed our two predictions.", 
  "Consistent with the first prediction, \u03b3 and the Hamann coefficient differed in several important ways in both the high-accuracy conditions.", 
  "In the 50-item condition, \u03b3 yielded a highly skewed, leptokurtic distribution that was characterized by theoreti- cally unexpected scores.", 
  "Gamma also yielded a higher estimate of monitoring accu- racy than the Hamann coefficient.", 
  "This finding is consistent with analytic claims made in the literature that \u03b3 yields a maximally liberal estimate of monitoring accuracy because it is computed without regard to marginal values (Reynolds, 1977).", 
  "In the 1,000-item condition, \u03b3 yielded a normal distribution that once again yielded a higher mean estimate of monitoring accuracy than the Hamann coefficient.", 
  "Consistent with the second prediction, \u03b3 and the Hamann coefficient yielded normal distributions that did not differ from each other with respect to their means, skewness, or kurtosis in the chance-accuracy condition.", 
  "This finding is consistent that of with Schraw (1995, p. 332), who showed that \u03b3 and the Hamann coefficient yield identical estimates when there is no relationship between recognition of a test item and monitoring judgments.", 
  "However, it was unexpected to find that the sampling variability of \u03b3 was roughly dou- ble that of the Hamann coefficient across all conditions in the study.", 
  "These findings suggest several general conclusions.", 
  "One is that the Hamann coeffi- cient appears more likely to yield a normally distributed sampling distribution in real- life testing situations in which test takers are reasonably accurate when completing a test with 50 or fewer items.", 
  "Testing the Hamann coefficient for statistical significance using an approximation to the normal distribution may be reasonable under these cir- cumstances.", 
  "In contrast, \u03b3 yields a nonnormal distribution that violates kurtosis and skewness assumptions and yields a positively biased estimate of monitoring accuracy.", 
  "As shown above, significance tests of \u03b3 based on the normal curve are quite conserva- tive and will yield high Type II error rates.", 
  "We recommend that the significance test outlined by Siegel and Castellan (1988) be used with great caution, if at all.", 
  "If applied researchers do choose to conduct significance tests, our findings suggest that a more liberal \u03b1 level may be appropriate.", 
  "For example, adopting a one-tailed \u03b1 value of .20 for the 20- and 50-item accurate monitoring conditions led to z-statistic rejection rates that were virtually identical to the expected rates on the basis of the empirical sam- \n\fNietfeld et al.", 
  "/ Relative and Absolute Monitoring Accuracy \n\npling distributions.", 
  "However, we believe that such a liberal recommendation itself speaks to the inherent problems associated with the use of \u03b3 for monitoring research under these conditions.", 
  "A second conclusion is that both statistics yield approximately normal distribu- tions when completing a lengthy test in which test takers are reasonably accurate.", 
  "Dif- ferences remain between the means of the two distributions that may have important interpretative consequences, even though both distributions are similar with respect to kurtosis and skewness.", 
  "Thus, choosing either \u03b3 or the Hamann coefficient under these conditions should be based on whether one prefers a liberal (i.e., \u03b3) or conservative (i.e., the Hamann coefficient) estimate of monitoring accuracy.", 
  "Additional research is needed to better determine how long a test must be before \u03b3 yields normally distributed \nMean differences aside, researchers also need to consider sampling variability of the two coefficients when making analytic decisions.", 
  "Across all conditions we stud- ied, the standard deviation of the \u03b3 sampling distribution was nearly twice as large as that of the Hamann coefficient.", 
  "Recall that we simulated a situation in which all test takers were homogeneous with respect to accuracy.", 
  "This being the case, an effective statistic should yield highly consistent estimates of monitoring accuracy across examinees.", 
  "This was clearly the case for the Hamann coefficient but not for \u03b3.", 
  "In prac- tice, this suggests that assessments of monitoring accuracy will be far more reliable when using the Hamann coefficient.", 
  "Because the 2:1 ratio in the sampling variability was observed in both the accurate and chance conditions, it is reasonable to expect that this finding would hold in more realistic situations where examinees differ greatly in their underlying accuracy.", 
  "In sum, we believe that these findings suggest some general direction for research- ers seeking a better understanding of the strengths and weaknesses to consider when choosing an appropriate index to measure monitoring accuracy.", 
  "In cases of a 2 \u00d7 2 data array when the test length is limited to 50 or fewer items, as found in a typical experi- mental setting, the Hamann coefficient appears to provide a more reliable estimate of monitoring accuracy.", 
  "Differences in the distributional properties between \u03b3 and the Hamann coefficient appear to be relatively small when sample size is significantly increased or when the level of accuracy is at chance occurrence, although \u03b3 retains greater sampling variability.", 
  "In addition, it appears that given equivalent conditions, \u03b3 will provide a higher estimate of monitoring accuracy than the Hamann coefficient.", 
  "However, the Hamann coefficient is limited in that it can be computed only when a data array has equal dimensions, whereas \u03b3 can be extended to unequal arrays.", 
  "In gen- eral, when considering the use of either index described in this study, we would rec- ommend that researchers consider two things before making their decisions.", 
  "The first is to consider the intent of the investigation while taking into account the context in which monitoring accuracy will be measured.", 
  "The second is to consider the appropri- ateness of a given index given what is known about its distributional qualities within the various constraints of the proposed study.", 
  "These constraints include but may not be limited to available sample size and difficulty of the material to be used in the investigation.", 
  "Educational and Psychological Measurement \nAs with any simulation study, generalizations beyond the conditions we examined should be made with caution.", 
  "For example, our simulations reflected a situation in which examinees where homogeneous with respect to accuracy.", 
  "Clearly, in applied settings, it is likely to be the case that examinees are more variable in this respect.", 
  "Fur- thermore, we examined only a small number of testing conditions.", 
  "For example, it is not known how these two statistics will perform with very short tests.", 
  "Similarly, it is not clear how lengthy a test needs to be for \u03b3 to yield more desirable distributional properties and correct significance tests.", 
  "Clearly, more research is needed that addresses these limitations before definitive statements about the use of these two coefficients can be made, but the current study does begin to clarify the relative strengths and weaknesses of these two measures of monitoring accuracy."
 ]
}