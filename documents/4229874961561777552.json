{
 "document_id": "4229874961561777552", 
 "sentences": [
  " An NEA policy brief  \nGlobal Competence Is a 21st Century Imperative \nThe 21st century isn\u2019t coming; it\u2019s already here.", 
  "And our students have the opportunity and challenge  of living and working in a diverse and rapidly changing world.", 
  "Public schools must prepare our young  people to understand and address global issues, and educators must re-examine their teaching  strategies and curriculum so that all students can thrive in this global and interdependent society.", 
  "\u2014NEA President Dennis Van Roekel \nThere is widespread recognition that a thorough  \nunderstanding of global issues is critical to the  United States\u2019 efforts to promote democratic prin- ciples and social justice, improve  our economic competitiveness, and  provide leadership in innovation  and creativity.", 
  "The National Education Association  \n(NEA) believes public education, the cornerstone of  our republic, is the only avenue through which today\u2019s  students\u2014tomorrow\u2019s workers\u2014can attain global  competence.", 
  "Our increasingly interconnected and interdependent  global society mandates that American students be  educated to develop habits of the mind that embrace  tolerance, a commitment to cooperation, an apprecia- tion of our common humanity, and a sense of responsi- bility\u2014key elements of global competence.", 
  "However,  not enough is being done in public schools and class- rooms to expose students to global issues.", 
  "Research  shows that most American students, low-income and  minority groups in particular, lag behind their peers in  other countries in their knowledge of world geography,  foreign languages, and cultures.1   \nWhat is global competence?", 
  "Global competence refers to the acquisition of in-depth  knowledge and understanding of international issues,  an appreciation of and ability to learn and work with  people from diverse linguistic and cultural backgrounds,  proficiency in a foreign language, and skills to function  \nproductively in an interdependent world community.", 
  "This  definition contains four basic elements:  \n \u25a0 International awareness.", 
  "This constitutes the knowl- edge and understanding of world history, socioeco- nomic and political systems, and other global events.", 
  "This awareness includes the understanding that local  and national events can have international implica- tions.", 
  "An individual who is aware of the broader world  environment also recognizes that an individual\u2019s ac- tions can affect others beyond one\u2019s own borders.", 
  "\u25a0 Appreciation of cultural diversity.", 
  "This entails the abil- \nity to know, understand, and appreciate people from  other cultures along with the capacity to acknowledge  other points of view about pressing world issues.", 
  "Awareness and appreciation of cross-cultural differ- ences, and the willingness to accept those differences,  opens doors for opportunities to engage in productive  and respectful cross-cultural relations.", 
  "\u25a0 Proficiency in foreign languages.", 
  "The ability to under- \nstand, read, write, and speak in more than one lan- guage enhances cross-cultural communication skills.", 
  "The knowledge of additional languages opens doors  to the understanding of other cultures and people who  speak those languages.", 
  "\u25a0 Competitive skills.", 
  "The ability to compete globally  \nentails the acquisition of extensive knowledge of inter- national issues.", 
  "To be able to compete, students need  high-level thinking skills that enhance creativity and  innovation.", 
  "Students who gain a thorough understand- ing of the economic, social, and technological changes  taking place across the globe enhance their ability to  compete in the worldwide marketplace.", 
  "NEA Education Policy and Practice Department | Center for Great Public Schools |  1201 16th St., NW, Washington, D.C. 20036       \n\f An NEA policy brief   An NEA policy brief  \nPromoting global competence for students   It is important that American students\u2014pre-school  through college\u2014begin developing a deeper under- standing of the world\u2019s economic, social, and political  issues.", 
  "A coalition for international education cautions  that \u201cglobal competence in the 21st century is not a  luxury, but a necessity.", 
  "Whether engaging the world, or  our culturally diverse homeland, the United States\u2019 future  success will rely on the global competence of our people.", 
  "Global competence must become part of the core mission  of education\u2014from K-12 through graduate school.\u201d2 \nWhy is this becoming such a pressing  concern for public education?", 
  "There are many factors that have led policy makers to  recognize that global competence is important for all  students: \n \u25a0 Economies are becoming more interdependent.", 
  "The  \nU.S. economy is becoming more globally connected.", 
  "Today, one in five jobs in this country is tied to interna- tional trade,3 and this trend is expected to continue.", 
  "\u25a0 American society is now more diverse.", 
  "American  \nsociety is becoming more linguistically and culturally  diverse.", 
  "In 2006, minority student enrollment in public  schools rose to 43 percent of the total student popula- tion\u2014up from 35 percent a decade earlier.4   \n \u25a0 Global challenges are becoming more complex.", 
  "Increasing global health and environmental challenges,  such as HIV/AIDS and H1N1 flu, natural disasters, global  warming, poverty, etc., call for coordinated global  responses.", 
  "With the outbreak of H1N1 flu, for example,  we have witnessed coordination of governments and  civil societies to address this issue.", 
  "Offering timely  responses to such challenges requires an understand- ing of other languages and cultures to facilitate easy  communication.", 
  "\u25a0 Global competence enhances overall academic  \nachievement.", 
  "Students acquire global competence  through the study of a second language, which is also  linked with enhanced cognitive development, higher- order thinking, and creativity.", 
  "Research shows that  students who study a foreign language score higher  in both the math and verbal portions of the ACT than  \ntheir counterparts who do not, and that students  across all socioeconomic levels who take a foreign  language perform better on the verbal section of SAT,  with more years of foreign language study associated  with higher scores.5 In addition, mastering the vocabu- lary of a foreign language enhances students\u2019 compre- hension and abilities in reading, writing, mathematics,  and other subjects.6 This implies that students taking  a foreign language\u2014at the elementary level through  secondary school\u2014are likely to achieve a greater level  of proficiency in a number of content areas.", 
  "NEA\u2019s partnerships to promote   global competence The National Education Association is committed to  strengthening the capabilities of America\u2019s public schools  to teach our students about global issues.", 
  "We are working  in collaboration with many other respected organizations  to address this issue:  \n \u25a0 Partnership for 21st Century Skills: The P21 brings  together the business community, education lead- ers, and policymakers to define a powerful vision for  21st century education that is aimed at ensuring that  every student is given the opportunity to succeed as a  worker and citizen in the 21st century.", 
  "\u25a0 Partnership for Global Learning: NEA, the Asia Society,  \nand five other national education organizations are  partners in a global learning network that provides  leadership and structure to help move international  education to the mainstream of American public educa- tion.", 
  "By connecting policy and practice, the partnership  aims to better prepare American students to thrive in  our interconnected world.", 
  "Policy approaches to promoting global  competence for educators To achieve global competence, America\u2019s public education  system must develop goals that provide equal education  opportunity for all students to realize their full potential.", 
  "Indeed, the Council of Chief State School Officers says that  our students must have \u201ccomplete access to a system of  education that recognizes and incorporates best practices  from around the globe, teaches skills and knowledge  necessary for success in the 21st century, and utilizes  \n\nNEA Education Policy and Practice Department | Center for Great Public Schools |  1201 16th St., NW, Washington, D.C. 20036       \n\f An NEA policy brief   An NEA policy brief  \nhigh quality and rigorous curricula, including foreign  languages and cultures.\u201d7 Clearly, global competence can  not be achieved without concerted efforts to train, recruit,  and support globally competent teachers and principals  for every classroom and every school.", 
  "Here are some steps that districts, states, and the federal  government can take to promote global competence  among educators: \n \u25a0 Align teacher preparation programs with global  \nperspectives.", 
  "This alignment offers educators the op- portunity to transfer the acquired skills into their class- rooms and schools.", 
  "Currently, a few universities such  as Indiana University, Michigan State University, Ohio  State University, and the University of Wisconsin offer  teacher preparation courses with a global approach.", 
  "At  the state level, three states\u2014California, North Caro- lina, and Ohio\u2014are starting to integrate international  perspectives into the classrooms.8  \n \u25a0 Design and support professional development pro- grams with a global focus.", 
  "States and school districts  must find ways to enhance educators\u2019 skills, pedagogi- cal practices, and content related to global compe- tence.", 
  "Delaware, Oklahoma, and Wisconsin are in the  forefront of providing such professional development  programs and opportunities for teachers.", 
  "In California,  an international studies project aims to support teach- ers in their quest for global competence.9  \n \u25a0 Find new ways to foster international exchanges.", 
  "Relatively few students and teachers are afforded the  opportunity to study abroad.", 
  "In 2009, for example, less  than one percent of America\u2019s high school or college  students participated in a study abroad program, with  students preparing to become teachers constituting  a small percentage of that group.", 
  "States and dis- tricts need to do more to promote virtual exchanges  between students and teachers from other countries  through the use of email and social media such as  blogs and video conferencing.", 
  "\u25a0 Expand the teaching of foreign languages.", 
  "Increase  \nschools\u2019 capacity to teach foreign languages, including  languages that may be less commonly used but are  considered critical to the nation\u2019s competitiveness and  national security.", 
  "A few states, such as Maryland, Min- \nnesota, Oregon, and Utah, are stepping up support for  foreign language study programs in public schools.", 
  "\u25a0 Benchmark educational systems, standards, and  community/family support systems against high  achieving countries.", 
  "The federal government can  provide incentives to states and local school districts  to internationally benchmark their education systems  and standards against high achieving nations.", 
  "The top  performing countries have achieved universally high  standards by investing in teacher and principal prepa- ration and recruitment; by promoting equity, diversity,  and individualized learning; and by combining univer- sal standards with substantial autonomy for schools.11"
 ]
}