{
 "document_id": "9210038794518361499", 
 "sentences": [
  "248Copyright \u00a9 2011, IGI Global.", 
  "Copying or distributing in print or electronic forms without written permission of IGI Global is prohibited.DOI: 10.4018/978-1-60960-153-9.ch014Chapter 14Learner-Centered Teaching and the Use of TechnologyAnnette GreerEast Carolina University, USAVivian W. MottEast Carolina University, USAINTRODUCTIONA mere mention the word \u201ctechnology\u201d often renders anxiety in instructors and students alike.", 
  "However, the term \u201ctechnology\u201d can represent the simplest of tools to aid the learning process.", 
  "For instance, a pencil with an eraser on the end, a chalk board and chalk, or even a Chinese abacus are instrumental technologies that we have used across time to facilitate the learning process.", 
  "Rela-tive to technology in the 21st century, the difference is the speed at which we are experiencing growth in both hardware (the pencil vs. computer) and software (writing vs. applications) in technological tools (Saba, 2001).Where do we find these technological tools in use in higher education?", 
  "Kennedy, Judd, Churchward, Gray, and Krause (2008) found that individuals who embrace emerging technologies in everyday life were more apt to be early adopters, using those same technologies in educational set-tings.", 
  "In higher education, we find technological tools inside the traditional classroom and among various created virtual learning environments generated by the technology itself\u2014in a place we often call cyberspace.", 
  "Technology permeates our global environment, offering tools that assist us in economic, social, and political dimensions.", 
  "ABSTRACTThis article explores the use of various learning technologies as tools for facilitating learner-centered teaching.", 
  "The article offers another perspective on the scholarship of teaching with technology\u2014through discussion of various theoretical models of learner-centered teaching, the role of technology on the student/instructor relationships, the impact on technology in different educational settings and contexts, and learners\u2019 cultural differences.", 
  "The article concludes with a brief discussion of future trends, cautions, and speculations related to technology use in learner-centered teaching.", 
  "249Learner-Centered Teaching and the Use of TechnologyTechnological tools are one of many elements that can aid communications and application, in education.", 
  "A closer look at the elements of educa-tion will help us in understanding the positive and negative value of technology as an educational resource and sometimes as the environment of learning.", 
  "The purposes of this article are threefold: first, to explore critical educational elements and the role educational technologies play in these elements; second, to consider how key theoretical models of learning are impacted by technology; and, third, to examine how instructors and learners variously respond to educational technologies and the impact of technological use on both instructor/learner relationships and learning.", 
  "Ultimately, the article seeks to add to the scholarship of teaching through an evidence-based review of the contex-tual influence of technology in education.BACKGROUND: EDUCATIONAL ELEMENTSThere are many elements to be considered in any educational environment: instructors, learners, content, delivery, application, context, environ-ment, and resources.", 
  "Not only are the elements essential to understand; further, the interaction of these elements, within the context of time and the roles that each of these elements play, have varied across historical time as higher education has evolved as well.", 
  "The relationship between the elements of education changes rapidly as the elements themselves have transformed with time and as knowledge has both become obsolete and expanded exponentially (Billings & Halstead, 2005; Mott, 2009).", 
  "It is the changing relationship among the educational elements of instructors, students, content, environment, all influenced by the application of technology resources, that have stimulated this article.Demonstration of technological competencies is central to the ability of instructors to meet the diverse learning needs of students.", 
  "Technological competencies are based on the same continuum as the technology itself from simple to complex ac-cording to the International Society for Technology in Education (ISTE, 2008).", 
  "ISTE has set national technology competency standards for instructors and students.", 
  "As early as 1995, ISTE commis-sioned a white paper regarding the technological literacy skills needed for the 21st century (Thomas & Knezek, 1995).", 
  "Technological literacy includes: (a) understanding math and science concepts underlying technological systems, (b) operability relative to various systems, (c) utilization and evaluation of diverse applications, (d) ability to innovate technology to solve emerging science problems, (e) awareness of the role of technology to any given career, (f) responsiveness to critical factors that lend success to any given career, and (g) appreciation of the role technology has on the various cultures of our global society.As with any competency, technological com-petency can be considered as three distinct skill areas: basic, professional, and application of technology in instruction.", 
  "Basic technological skills include the introductory level of function that is the operation of applications for personal communications.", 
  "These applications can include e-mail, basic blogs, word processing, spread-sheets for home use, and simple presentations, for example.", 
  "Professional technological skills include higher, intermediate levels for professional communications.", 
  "E-mails, for example, used professionally require knowledge of embedded applications such as certified signature use, timed release, tracking delivery and opened status, and automation of e-mail rules for organization and management.", 
  "Another example would be use of the word processing at the professional level to track changes, merge documents, insert citations and references, automate a table of contents, and import from other applications into the word processor.", 
  "At the advanced competency level, professionals using technology to teach would have evolved from basic to professional intermediate, and are poised to expand and transfer existing skills into \f  \n14 more pages are available in the full version of this document, which may \nbe purchased using the \"Add to Cart\" button on the product's webpage: \nwww.igi-global.com/chapter/learner-centered-teaching-use- \ntechnology/49307?camid=4v1 \nThis title is available in InfoSci-Educational Technologies, InfoSci-Books, Business-Technology-Solution, Library Science, Information Studies, and Education, InfoSci-Select, InfoSci-Educational Science and Technology, \nInfoSci-Select.", 
  "Recommend this product to your librarian: \nwww.igi-global.com/e-resources/library-recommendation/?id=13 \nRelated Content \nAcademic Workload in Online Courses Geoffrey N. Dick (2009).", 
  "Encyclopedia of Distance Learning, Second Edition (pp.", 
  "1-6).", 
  "www.igi-global.com/chapter/academic-workload-online-courses/11728?camid=4v1a \nResearch on the E-Teacher in the K-12 Distance Education Classroom Elizabeth Murphy and Mar\u00eda A. Rodr\u00edguez-Manzanares (2009).", 
  "Encyclopedia of Distance Learning, Second Edition (pp.", 
  "1744-1778).", 
  "www.igi-global.com/chapter/research-teacher-distance-education-classroom/11988?camid=4v1a \nI Scratch and Sense But Can I Program?", 
  ": An Investigation of Learning with a Block Based Programming Language N. K. Simpkins (2014).", 
  "International Journal of Information and Communication Technology Education (pp.", 
  "www.igi-global.com/article/i-scratch-and-sense-but-can-i-program/117279?camid=4v1a \nAdaptive E-Learning Environments: Research Dimensions and Technological Approaches Pierpaolo Di Bitonto, Teresa Roselli, Veronica Rossano and Maria Sinatra (2013).", 
  "International Journal of Distance Education Technologies (pp.", 
  "1-11).", 
  "www.igi-global.com/article/adaptive-e-learning-environments/83512?camid=4v1a"
 ]
}