{
 "document_id": "1171579.1", 
 "sentences": [
  "Initiation, Response, Follow-up, and Beyond:  An Analysis of Discourse in the Tutorial Setting  \nIn order to assure equitable educational opportunity for all students, many need the kind  \nof individualized instruction that tutoring offers.", 
  "More than 30 years ago, Cohen, Kulik, and  Kulik (1982) reported findings from a meta-analysis that supported their claim of the  effectiveness of one-to-one tutoring.", 
  "Additional meta-analyses have followed (Elbaum, Vaughn,  Hughes, & Moody, 2000; Shenderovich, Thurston, & Miller, 2016).", 
  "However, none of these  reviews forged the link between the results obtained and the quality of tutoring discourse.", 
  "In  reviewing the tutoring literature more broadly, I found only seven studies that included excerpts  from tutor/student discourse in which the student exhibited difficulty and the dialogue moved  toward the resolution of that difficulty.", 
  "Three of the studies dealt with tutoring at the first grade  level (Pinnell, Lyons, Defore, Bryk, & Selzer, 1994; Rodgers, 2004/2005; Timmons & Morgan,  2010).", 
  "Four of the studies described tutoring, predominantly or exclusively, at the third through  sixth grade level (Almazrouhi, 2007; Dawes, 2007; Hedin & Gaffney, 2013; Newcomer, 2010).", 
  "tutor and two students.", 
  "Specifically, the research serving as the foundation for this paper  examines the difficulties vulnerable readers1 exhibited as they read extended text during tutoring,  the responses made by the tutor to those difficulties, and the way in which the tutoring path  moving forward seemed to be influenced by those responses.", 
  "I argue here that dialogue crafted  in this interaction differs from that of tutoring described in the research literature.", 
  "The purpose of this paper is to describe and analyze the discourse occurring between a  \nTheoretical Framework  \nIn his seminal essay The Problem of Speech Genres, Bakhtin (1986), had this to say about  dialogue between parties:  \n(W)hen the listener perceives and understands the meaning (the language meaning) of  speech, he simultaneously takes an active, responsive attitude towards it.", 
  ".", 
  ".", 
  ".", 
  "Any  understanding is imbued with response and necessarily elicits it in one form or another:   the listener becomes the speaker\u201d (p. 68)  \nDialogue differs from presentation:  each utterance presumes that the interlocutor will take it in,  turn it over in her/his mind, and decide how to respond; each act of listening presumes that a  response will be made, the interlocutor will consider it, and reply in turn.", 
  "As Holland,  Lachicotte, Skinner, and Cain (1998) assert, a dialogic interaction is one in which \u201csentient  beings always exist in a state of being \u2018addressed\u2019 and in the process of \u2018answering\u2019\u201d (p. 169).", 
  "And the import of what is spoken\u2014and left unsaid\u2014goes beyond the desire to for  understanding.", 
  "Rather, discourse serves to craft and re-craft the conditions of a given setting by  establishing typical patterns of communication.", 
  "Patterns like this exist in all contexts, including  that of instruction about the reading process in school.", 
  "The discourse occurring within these contexts is most commonly framed by the actions  \nand speech of the adults that inhabit them.", 
  "As Johnstone (2002) describes it teachers \u201cdecide  what will be talked about in what way, and for what purpose\u201d (p. 121).", 
  "We might imagine an  instructional environment differing, in many respects, from the one Johnstone (2002) describes.", 
  "1 I use this term in reference to readers who are unusually sensitive to disruptions in their literacy ecology such as  uninteresting texts, inauthentic tasks, and a range of potentially oppressive class, race, and gender factors (Author,  \n\fDozier and Rutton (2005) propose the term responsive teaching in reference to such a  phenomenon.", 
  "In this context, \u201cteaching involves building on learners\u2019 responses, fostering  flexibility, setting clear goals for instruction and developing teacher-student-teacher exchange of  ideas\u201d (p. 461).", 
  "This is no curricular free-for-all; at the macro level, overarching goals (ideally  developed in collaboration with students) structure teaching and learning, but, at the micro level,  interactions are fundamentally dialogic (Wells, 1996).", 
  "Responsive teaching evokes a setting  distinctly different from that in which teachers proceed through the curriculum with minimal  attention paid to what children have to say.", 
  "In the tutoring context, teaching and learning can be more closely tailored to meet the  \nindividual needs of the child involved and the possibility for true responsivity more easily  realized.", 
  "However, research on responsive teaching is less than explicit in terms of practical  application, particularly in the tutorial setting.", 
  "Such specifics are sorely needed.", 
  "Research Methodology  \nThe site for this exploratory, IRB-approved study was an elementary school in a large  \nurban district in a western state which enrolled 350 students.", 
  "Four fourth grade students  participated in this study.", 
  "Sam and Bella (pseudonyms) were two of these children.", 
  "I served as  the tutor and researcher in the study.", 
  "Tutorial sessions occurred two times per week for 40  minutes each, for a total of 30 sessions per child.", 
  "Sam and Bella\u2019s specific challenges and  interests determined the content of the tutorial and varied over time.", 
  "The study was guided by the following research questions:  \n1.", 
  "What are the characteristics of discourse occurring in the transcripts analyzed in this  \nstudy of two fourth grade students and their tutor?", 
  "2.", 
  "In what ways do power dynamics play out in the interaction between tutor and students?", 
  "3.", 
  "In what ways are discourse characteristics linked to instructional sequences that end with  \nthe child coming to an understanding of the content with which s/he originally struggled?", 
  "4.", 
  "In what ways is the discourse that characterizes the tutoring sessions studied similar to  \nand different from that exhibited in the reviewed literature?", 
  "Data Sources  \nThe primary source of data for the analysis associated with this paper were start-to-finish  audio-recordings of each of our interactions.", 
  "After transcribing the audio-recordings and prior to  discourse analysis (Gee, 2011; Johnstone, 2002), I bracketed off the sections of the transcript  characterized by interactions initiated when the child struggled.", 
  "I employed in my analysis  terminology as defined by other discourse analysts (e.g., Wells, 1996); the most important of  these were move (a contribution to an exchange made by a participant in a single speaking turn),  nuclear exchange (a single initiation-response-follow-up interaction), and sequence (all the  moves required to fulfill or abandon the expectations set up by the initiating move).", 
  "The  categories and codes involved:  asking follow-up questions (repetitions/paraphrases, narrowing  questions, or requests for clarification), providing information (general verbal, visual, example,  sentence context, or answer), and taking action (demonstrating strategy use or moving on with no  response).", 
  "Reliability was established via both intra- and inter-rater coding.", 
  "Intra-rater  reliability was .99; inter-rater reliability was .91.", 
  "I was also interested in the ways in which  power relations played out in the tutoring interactions and coded for interruptions and rejections,  as well as tentative and supportive language and the presence of pauses.", 
  "Excerpts from the  articles included in the literature review were coded in the same way.", 
  "Results  \nIn this example of a tutoring protocol that resulted in strong gains for the children who  \nparticipated, discourse exhibited clear characteristics.", 
  "RQ1:  Moves that came as follow-ups to  the child\u2019s initial response consisted of questions, information, strategy demonstration/  discussion, provision of a possible answer, or lack of verbal reply.", 
  "Other than the latter, the  frequency of moves was quite equal.", 
  "The average number of moves within a single sequence  was approximately five; in most cases it didn\u2019t take a long time to come to resolution or move  on.", 
  "When looking across the whole range of moves making up a sequence, there was  considerable variety; it was uncommon for a child to be peppered with question after question or  provided with more information than s/he could constructively employ.", 
  "RQ2:   In terms of  power dynamics, while there were instances of interruption and explicit rejection of a child\u2019s  response, the use of tentative language, supportive comments, and willingness to sit in silence as  appropriate were more common.", 
  "RQ3:  Given that just over half of all sequences ended with the  child coming to an appropriate resolution\u2014or, in many cases, one of a number of appropriate  resolutions\u2014of the discussion, it appears that finding the perfect way to react to a child\u2019s  comment is less crucial than might have been thought; attending carefully and replying in ways  that reflect that attention appear more important.", 
  "RQ4:  Findings from the research literature  were quite different.", 
  "Tutors in those studies exhibited a limited range of follow-up moves, falling  into a sort of tutoring script rather than responding intentionally to what children say in the  moment.", 
  "one in which tutors decide what is studied and how, diminishing student agency, and, potentially,  leading to a passive reader identity.", 
  "The context embraced by this study is quite different.", 
  "Follow-up moves, at the third-turn position and beyond, come in response to what children say  and what they do not say, depending both on what the tutor hears in the moment and what s/he  has come to learn about the child over time.", 
  "The instructional context that might be imagined as one reads the research literature is  \nStudy Significance  \nThis study is significant in that it links the quality of tutor/student discourse to the overall  success of the tutoring enterprise, providing support for the claim that it is important to attend to  specific tutor/student interactions as well as the content and structure of the tutoring protocol  when evaluating the quality of the instruction.", 
  "It also has implications for practice.", 
  "Tutoring  interactions should demonstrate both variety and balance.", 
  "Any effort to avoid doing one  particular thing\u2014whether that be questioning, information giving, or modeling\u2014is worth the  extra effort.", 
  "It is also important to consider that, under some circumstances such as dealing with  an unknown vocabulary word while focusing on formulating opinions, simply providing the  student with an answer and moving on may be the best approach; longer dialogue sequences are  not always more productive.", 
  "Maintaining positive morale is a worthy goal, but, a balance of  more and less successful instructional sequences appears adequate; alternating work on topics  that challenge and that come more easily may prove effective.", 
  "There are implications for teacher  education as well.", 
  "Providing pre-service and practicing teachers with tutoring opportunities will  support them in crafting productive interactions with students.", 
  "Finally, there are implications for  future research.", 
  "This study presents information about interactions around student difficulty;  analysis of response to student success may be just as beneficial.", 
  "Another study might  investigate the effects of teaching tutors to analyze audio-recordings of their tutoring sessions,  selecting one area of improvement they wish to work on, and noting the apparent impact of those  changes on the relationships they build with the children they serve."
 ]
}